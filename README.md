This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Excercise

The goal is to create a small React app & Node.js microservice that performs the following steps:
* Microservice:
   - Fetches data from a public API
   - Transforms and returns the data in JSON format through HTTP API to the React App
* React app:
   - Presents the data returned by HTTP API  in a user-friendly form (React app)
Node.js microservice requirements:
API to use: http://api.population.io
Data to fetch: population and life expectancy for three countries
Transforms/calculations:
- total population today per country
- average life expectancy for persons born on 1952-01-01 per country
You should use:
- Node.js version 8.12 or newer
- ES6 syntax
- Promises
You can choose other frameworks & libraries freely (e.g. Express, lodash, moment)
React app requirements:
You should use:
- ES6 syntax, Promises
- Webpack
- Yarn
- React


## How to run

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `yarn server`

Start Express based micro-service with default URI of http://localhost:3333

