import axios from 'axios';
import {WORLD_POPULATION_API} from "./configuration";

export const countriesClient = {
  getAvailableCountries: () => axios.get(`${WORLD_POPULATION_API}/countries`)
};