export const populationDataToTotalPopulationPerCountry = countryPopulationData => {
  if (!countryPopulationData.length) return {country: null, totalPopulationPerCountry: null};

  const targetCountry = countryPopulationData[0].country;
  let totalPopulationPerCountry = 0;

  for (let ageBracket of countryPopulationData){
    totalPopulationPerCountry = totalPopulationPerCountry + ageBracket.males + ageBracket.females;
  }

  return {
    country: targetCountry,
    total_population: totalPopulationPerCountry
  }

};