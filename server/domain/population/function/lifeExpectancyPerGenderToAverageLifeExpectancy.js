export const lifeExpectancyPerGenderToAverageLifeExpectancy = dataPerGender => {
  const averageData = {
    // country: {males, females}. Typescript and tests are essential in such cases
  };

  for (let entry of dataPerGender){
    if (averageData.hasOwnProperty(entry.country)){
      averageData[entry.country][entry.sex] = entry.total_life_expectancy;
    } else {
      averageData[entry.country] = {
        [entry.sex]: entry.total_life_expectancy
      }
    }
  }
  return Object.entries(averageData).map(entry => {
    return {
      country: getCountryFromEntry(entry),
      average_life_expectancy: getAverageLifeExpectancyFromEntry(entry)
    }
  });
};

const getCountryFromEntry = entry => entry[0];

const getAverageLifeExpectancyFromEntry = entry => {
  return (entry[1].male + entry[1].female) / 2;
};