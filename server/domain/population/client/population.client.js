import axios from 'axios';
import {WORLD_POPULATION_API} from "../../../configuration";

const POPULATION_API = `${WORLD_POPULATION_API}/population`;

export const populationClient = {
  getTotalPopulationForYearAndCountry: (year, country) => axios.get(`${POPULATION_API}/${year}/${country}`)
};
