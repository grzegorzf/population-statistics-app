import {populationClient} from "../client";
import {populationDataToTotalPopulationPerCountry} from "../function";
import axios from "axios";

export const populationController = {

  getTotalPopulationForCountries: (req, res) => {

    const currentYear = new Date().getFullYear();
    const {countries: rawCountries} = req.query;

    const countries = JSON.parse(rawCountries);

    if (!Array.isArray(countries) || (Array.isArray(countries) && countries.length !== 3)) {
      res.status(400).json({message: "Wrong amount of countries provided."});
    }

    // TODO -> should be handler in more generic (and that hard-coded) fashion like in life-expectancy.controller

    axios.all([
      populationClient.getTotalPopulationForYearAndCountry(currentYear, countries[0]),
      populationClient.getTotalPopulationForYearAndCountry(currentYear, countries[1]),
      populationClient.getTotalPopulationForYearAndCountry(currentYear, countries[2]),
    ])
    .then(axios.spread((country1, country2, country3) => {
      res.json([
          populationDataToTotalPopulationPerCountry(country1.data),
          populationDataToTotalPopulationPerCountry(country2.data),
          populationDataToTotalPopulationPerCountry(country3.data)
        ]);
    }))
    .catch(reason => {
      res.status(500).json({message: "Request could not be handled.", errorDetails: reason})
    });
  },

  getTotalPopulationForCountry: (req, res) => {

    const currentYear = new Date().getFullYear();
    const country = res.params.country;

    populationClient.getTotalPopulationForYearAndCountry(currentYear, country)
      .then(({data}) => res.json({data}))
      .catch(({response}) => res.status(500).json({
        message: "Request could not be handled.",
        errorDetails: response.detail
      }))
  },

};
