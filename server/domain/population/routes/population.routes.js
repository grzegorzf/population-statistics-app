import express from 'express';
import {populationController} from "../controller/population.controller";

export const populationRouter = express.Router();

populationRouter.use(function(req, res, next) {
  console.log('Population endpoint handler fired.');
  next();
});

populationRouter.get('/details', populationController.getTotalPopulationForCountries);
// populationRouter.get('/{country}', populationController.getTotalPopulationForCountry);
