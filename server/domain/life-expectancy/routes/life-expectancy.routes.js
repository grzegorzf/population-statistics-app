import express from 'express';
import {lifeExpectancyController} from "../controller";

export const lifeExpectancyRouter = express.Router();

lifeExpectancyRouter.use((req, res, next) => {
  console.log('Life expectancy endpoint handler fired.');
  next();
});

lifeExpectancyRouter.get('/details', lifeExpectancyController.getAverageLifeExpectancyForACountriesAndDateOfBirth);
// lifeExpectancyRouter.get('/{country}/{dob}', lifeExpectancyController.getAverageLifeExpectancyForACountryAndDateOfBirth);
