import axios from 'axios';
import {WORLD_POPULATION_API} from "../../../configuration";

const LIFE_EXPECTANCY_API = `${WORLD_POPULATION_API}/life-expectancy`;

export const lifeExpectancyClient = {
  getTotalLifeExpectancyForSexCountryAndDob: (sex, country, dob) =>  axios.get(`${LIFE_EXPECTANCY_API}/total/${sex}/${country}/${dob}/`)
};