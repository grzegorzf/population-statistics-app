import {lifeExpectancyClient} from "../client/life-expectancy.client";
import {Gender} from "../../../cross_domain/type";
import {lifeExpectancyPerGenderToAverageLifeExpectancy} from "../../population/function";
import axios from 'axios';

export const lifeExpectancyController = {

  getAverageLifeExpectancyForACountriesAndDateOfBirth: (req, res) => {

    const {countries : rawCountries, dob} = req.query;
    const promises = [];
    const countries = JSON.parse(rawCountries);

    if (!Array.isArray(countries) || (Array.isArray(countries) && countries.length !== 3)) {
      res.status(400).json({message: "Wrong amount of countries provided."});
      return;
    }

    for (let country of countries){
      promises.push(lifeExpectancyClient.getTotalLifeExpectancyForSexCountryAndDob.bind(this, Gender.MALE, country, dob));
      promises.push(lifeExpectancyClient.getTotalLifeExpectancyForSexCountryAndDob.bind(this, Gender.FEMALE, country, dob));
    }

    axios.all([...promises.map(promise => promise())])
    .then(axios.spread((...responses) => {
      res.json(lifeExpectancyPerGenderToAverageLifeExpectancy(responses.map(r => r.data)));
    }))
    .catch(reason => {
      res.status(500).json({message: "Request could not be handled.", errorDetails: reason.response.detail})
    });

  },

  getAverageLifeExpectancyForACountryAndDateOfBirth: (req, res) => {
    const {country, dob} = req.params;

    axios.all([
      lifeExpectancyClient.getTotalLifeExpectancyForSexCountryAndDob(Gender.MALE, country, dob),
      lifeExpectancyClient.getTotalLifeExpectancyForSexCountryAndDob(Gender.FEMALE, country, dob),
    ])
    .then(axios.spread((maleExpectancy, femaleExpectancy) => {
      res.json({
        maleExpectancy,
        femaleExpectancy
      });
    }))
    .catch(reason => {
      res.status(500).json({message: "Request could not be handled.", errorDetails: reason.response.detail})
    })
  }

};