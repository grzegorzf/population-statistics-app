import axios from 'axios'
import {SERVER_HOSTNAME} from "../../../../configuration";

export const statisticsClient = {
  getAvailableCountries : () => axios.get("http://api.population.io:80/1.0/countries"),
  fetchTotalPopulationForCountries: countries => axios.get(`${SERVER_HOSTNAME}/population/details?countries=${countries}`),
  fetchAverageLifeExpectancyForACountryAndDateOfBirth: (countries, dob = "1952-01-01") =>
      axios.get(`${SERVER_HOSTNAME}/life-expectancy/details?countries=${countries}&dob=${dob}`),
  fetchTotalPopulationForCountry: country => axios.get(`${SERVER_HOSTNAME}/population/${country}`),
};
