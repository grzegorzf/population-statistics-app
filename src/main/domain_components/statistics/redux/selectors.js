import React from "react";

export const selectAvailableCountries = state => state.statistics.availableCountries;

export const selectSelectedCountries = state => state.statistics.selectedCountries;

export const selectCountryDetails = state => state.statistics.countryDetails;

export const selectCountryDetailsCount = state => Object.keys(state.statistics.countryDetails).length;

export const selectAvailableCountriesCount = state => state.statistics.availableCountries.length;

export const selectCountryDetailsAsAnArray = state => {
  return Object.entries(state.statistics.countryDetails).map(entry => {
    return {
      country: entry[0],
      total_population: entry[1].total_population,
      average_life_expectancy: entry[1].average_life_expectancy
    }
  });
};

