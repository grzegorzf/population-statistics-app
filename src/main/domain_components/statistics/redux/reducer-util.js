export const saveOrUpdatePopulationData = (populationDetails, countryDetails) => {
  for (let entry of populationDetails){
    if (countryDetails.hasOwnProperty(entry.country)){
      countryDetails[entry.country].total_population = entry.total_population;
    } else {
      countryDetails[entry.country] = {
        total_population: entry.total_population
      }
    }
  }
};

export const saveOrUpdateLifeExpectancyData = (lifeExpectancyDetails, countryDetails) => {
  for (let entry of lifeExpectancyDetails){
    if (countryDetails.hasOwnProperty(entry.country)){
      countryDetails[entry.country].average_life_expectancy = entry.average_life_expectancy;
    } else {
      countryDetails[entry.country] = {
        average_life_expectancy: entry.average_life_expectancy
      }
    }
  }
};