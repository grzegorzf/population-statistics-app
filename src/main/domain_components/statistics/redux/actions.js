import {
  SET_AVAILABLE_COUNTRIES,
  SET_SELECTED_COUNTRIES,
  CLEAR_SELECTED_COUNTRIES,
  SET_POPULATION_DETAILS,
} from './actionTypes';
import {statisticsClient} from "../client";

export function fetchAvailableCountries(){
  return function(dispatch){
    return statisticsClient.getAvailableCountries()
        .then(({data}) => dispatch(setAvailableCountries(data.countries)))
  }
}

export function fetchPopulationDetails(country){
  return function(dispatch){
    return statisticsClient.fetchTotalPopulationForCounties(country)
        .then(response => dispatch(setPopulationDetails(response)))
  }
}

export function setPopulationDetails(populationDetails, lifeExpectancyDetails){
  return {
    type: SET_POPULATION_DETAILS,
    payload: {populationDetails, lifeExpectancyDetails}
  }
}

export function setAvailableCountries(countries){
  return {
    type: SET_AVAILABLE_COUNTRIES,
    payload: {countries}
  }
}

export function setSelectedCountries(selectedCountries){
  return {
    type: SET_SELECTED_COUNTRIES,
    payload: {selectedCountries}
  }
}

export function clearSelectedCountries(){
  return {
    type: CLEAR_SELECTED_COUNTRIES
  }
}

export const fetchPopulationDetailsForCountries = (countries) => {
  return async (dispatch) => {
    try {
      const populationData = await statisticsClient.fetchTotalPopulationForCountries(countries);
      const lifeExpectancyData = await statisticsClient.fetchAverageLifeExpectancyForACountryAndDateOfBirth(countries);
      dispatch(setPopulationDetails(populationData.data, lifeExpectancyData.data));
    } catch (e) {
      //
    }
  }
};