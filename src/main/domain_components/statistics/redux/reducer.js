import {
  SET_SELECTED_COUNTRIES,
  SET_AVAILABLE_COUNTRIES,
  CLEAR_SELECTED_COUNTRIES,
  SET_POPULATION_DETAILS
} from './actionTypes';
import {statisticsInitialState} from './initialState';
import {saveOrUpdateLifeExpectancyData, saveOrUpdatePopulationData} from "./reducer-util";

export const statisticsReducer = (state = statisticsInitialState, action = {}) => {
  const {payload} = action;
  switch (action.type){

    case SET_POPULATION_DETAILS:
      const {populationDetails, lifeExpectancyDetails} = payload;
      const countryDetails = Object.assign({}, state.countryDetails);

      saveOrUpdatePopulationData(populationDetails, countryDetails);
      saveOrUpdateLifeExpectancyData(lifeExpectancyDetails, countryDetails);

      return {
        ...state,
        countryDetails
      };

    case SET_SELECTED_COUNTRIES:
      const {selectedCountries} = payload;
      return {
        ...state,
        selectedCountries
      };

    case SET_AVAILABLE_COUNTRIES:
      const {countries} = payload;
      return {
        ...state,
        availableCountries: countries
      };

    case CLEAR_SELECTED_COUNTRIES:
      return {
        ...state,
        selectedCountries: []
      };

    default:
      return state;

  }
};

