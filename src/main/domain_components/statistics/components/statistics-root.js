import React from 'react'
import {Route, Switch} from "react-router-dom";

import {ConnectedCountriesSelectionPage} from "./pages";
import {ConnectedSelectedCountriesDetailsPage} from "./pages/selected_countries_details_page/selected-countries-details-page.connected";

export const StatisticsRoot = ({match}) => (
    <Switch>
      <Route path={match.url + "/countries-selection"} component={ConnectedCountriesSelectionPage}/>
      <Route path={match.url + "/details"} component={ConnectedSelectedCountriesDetailsPage}/>
      <Route exact path={match.url} component={ConnectedCountriesSelectionPage}/>
    </Switch>
);