import React from 'react'
import './country-data.css'

export const CountryData = ({country, totalPopulation, averageLifeExpectancy}) => (
  <div className="country-data">
    <h2>{country}</h2>
    <p>Total population:</p>
    <h4>{totalPopulation}</h4>
    <p>Average life expectancy:</p>
    <h4>{averageLifeExpectancy.toFixed(2)}</h4>
  </div>
);
