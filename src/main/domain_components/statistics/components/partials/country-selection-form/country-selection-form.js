import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './country-selection-form.css'
export class CountrySelectionForm extends Component {

  renderSelectionPanel = () => {
    const {countries, onSelect, selectedCountries} = this.props;
    return countries.map(country => {
      return (
        <label
          key={country}
          htmlFor={country}>
          <input
            id={country}
            onChange={onSelect}
            checked={selectedCountries.includes(country)}
            type="checkbox"
            value={country}
          />
          {country}
        </label>
      )
    })
  };

  render(){
    return (
      <form>
        {this.renderSelectionPanel()}
      </form>
    )
  }
}

CountrySelectionForm.propTypes = {
  countries: PropTypes.array,
  selectedCountries: PropTypes.array,
  onSelect: PropTypes.func
};