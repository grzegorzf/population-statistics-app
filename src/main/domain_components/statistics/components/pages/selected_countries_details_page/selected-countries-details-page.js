import React, { Component } from 'react';
import {CountryData} from "../../partials/country-data";

export class SelectedCountriesDetailsPage extends Component {

  state = {
    selectedCountries: []
  };

  renderCountryDetails = () => {
    const {countryDetailsAsAnArray} = this.props;
    return countryDetailsAsAnArray.map(entry => (
      <CountryData
        country={entry.country}
        totalPopulation={entry.total_population}
        averageLifeExpectancy={entry.average_life_expectancy}
      />
    ))
  };

  render() {
    const {countryDetailsCount} = this.props;
    if (!countryDetailsCount) return <h3>No country details available.</h3>;
    return (
      <div className="selected-countries-details-page">
        <h3>Country details for: {countryDetailsCount} </h3>
        <div className="country-data-list">
          {this.renderCountryDetails()}
        </div>
      </div>
    );
  }
}


