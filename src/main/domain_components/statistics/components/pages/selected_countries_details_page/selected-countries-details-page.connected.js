import {connect} from 'react-redux'
import {withRouter} from "react-router";
import {SelectedCountriesDetailsPage} from "./selected-countries-details-page";
import {selectCountryDetailsAsAnArray, selectCountryDetailsCount,} from "../../../redux/selectors";

const mapStateToProps = (state) => (
    {
      countryDetailsAsAnArray: selectCountryDetailsAsAnArray(state),
      countryDetailsCount: selectCountryDetailsCount(state)
    }
);

const mapDispatchToProps = dispatch => (
    {}
);

export const ConnectedSelectedCountriesDetailsPage =
    withRouter(connect(
        mapStateToProps,
        mapDispatchToProps,
    )(SelectedCountriesDetailsPage));
