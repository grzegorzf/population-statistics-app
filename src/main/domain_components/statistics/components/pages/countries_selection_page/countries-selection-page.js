import React, { Component } from 'react';
import {CountrySelectionForm} from "../../partials/country-selection-form";

export class CountriesSelectionPage extends Component {

  state = {
    selectedCountries: []
  };

  componentWillMount(){
    const {availableCountries, fetchAvailableCountries} = this.props;
    if (!availableCountries.length){
      fetchAvailableCountries();
    }
  }

  onSelect = ({target: {value, checked}}) => {
    const {selectedCountries} = this.state;
    if (checked) {
      if (selectedCountries.length >= 3) selectedCountries.pop();
      selectedCountries.push(value);
    }
    else selectedCountries.splice(selectedCountries.indexOf(value), 1);
    this.setState({selectedCountries});
  };

  isSubmitReady =() => {
    const {selectedCountries} = this.state;
    return selectedCountries.length === 3;
  };

  onSubmit = () => {
    const {selectedCountries} = this.state;
    const {fetchPopulationDetailsForSelectedCountries} = this.props;
    const encodedParam = encodeURIComponent(JSON.stringify(selectedCountries));
    fetchPopulationDetailsForSelectedCountries(encodedParam);
  };

  render() {
    const {availableCountries} = this.props;
    const {selectedCountries} = this.state;
    const submitReady = this.isSubmitReady();

    return (
      <div className="countries-selection">
        <h5>Select 3 countries and fetch their details</h5>
        <CountrySelectionForm
          onSelect={this.onSelect}
          countries={availableCountries}
          selectedCountries={selectedCountries}
        />
        <button
          type="button"
          disabled={!submitReady}
          onClick={this.onSubmit}>
          Fetch details for selected countries
        </button>
      </div>
    );
  }
}


