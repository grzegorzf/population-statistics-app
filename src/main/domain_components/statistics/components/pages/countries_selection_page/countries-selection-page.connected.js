import {connect} from 'react-redux'
import {CountriesSelectionPage} from "./countries-selection-page";
import {withRouter} from "react-router";
import {
  selectAvailableCountries,
  selectSelectedCountries,
} from "../../../redux/selectors";
import {fetchAvailableCountries, fetchPopulationDetailsForCountries, setAvailableCountries, setSelectedCountries} from "../../../redux";

const mapStateToProps = (state) => (
    {
      selectedCountries: selectSelectedCountries(state),
      availableCountries: selectAvailableCountries(state)
    }
);

const mapDispatchToProps = dispatch => (
    {
      fetchAvailableCountries: () => dispatch(fetchAvailableCountries()),
      fetchPopulationDetailsForSelectedCountries: countries => dispatch(fetchPopulationDetailsForCountries(countries)),
      setAvailableCountries: countries => dispatch(setAvailableCountries(countries)),
      setSelectedCountries: selectedCountries => (dispatch(setSelectedCountries(selectedCountries)))
    }
);

export const ConnectedCountriesSelectionPage =
    withRouter(connect(
        mapStateToProps,
        mapDispatchToProps,
    )(CountriesSelectionPage));
