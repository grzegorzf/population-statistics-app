import React, { Component } from 'react';
import {AppRoot} from "./app-root";
import {Navigation} from "../navigation";
import './app.css'

export class App extends Component {

  render() {
    const {availableCountriesCount, countryDetailsCount} = this.props;
    return (
      <div className="app">
        <Navigation
          availableCountriesCount={availableCountriesCount || 0}
          countryDetailsCount={countryDetailsCount || 0}
        />
        <AppRoot />
      </div>
    );
  }
}


