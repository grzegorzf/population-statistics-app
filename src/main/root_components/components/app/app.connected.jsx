import {App} from "./app";
import {connect} from 'react-redux'
import {withRouter} from "react-router";
import {
  selectAvailableCountriesCount,
  selectCountryDetailsCount
} from "../../../domain_components/statistics/redux";

const mapStateToProps = state => (
  {
    availableCountriesLength: selectAvailableCountriesCount(state),
    countryDetailsCount: selectCountryDetailsCount(state)
  }
);

const mapDispatchToProps = dispatch => (
    {
    }
);

export const ConnectedApp = withRouter(
    connect(
    mapStateToProps,
    mapDispatchToProps,
  )(App)
);
