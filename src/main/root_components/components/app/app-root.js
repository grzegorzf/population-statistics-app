import React from 'react'
import {Route, Switch} from "react-router-dom";
import {ConnectedCountriesSelectionPage} from "../../../domain_components/statistics";
import {ConnectedSelectedCountriesDetailsPage} from "../../../domain_components/statistics/components/pages/selected_countries_details_page";

export const AppRoot = () => (
    <Switch>
      <Route path={"/countries-selection"} component={ConnectedCountriesSelectionPage}/>
      <Route path={"/details"} component={ConnectedSelectedCountriesDetailsPage}/>
      <Route component={ConnectedCountriesSelectionPage}/>
    </Switch>
);