import React from 'react'
import {NavLink} from "react-router-dom";
import './navigation.css'
import PropTypes from "prop-types";

export const Navigation = ({availableCountriesCount, countryDetailsCount}) => (
  <nav>
    <NavLink activeClassName="active" to="/countries-selection">Countries selection <small> ({availableCountriesCount})</small></NavLink>
    <NavLink activeClassName="active" to="/details">Details <small> ({countryDetailsCount})</small></NavLink>
  </nav>
);

Navigation.propTypes = {
  availableCountriesCount: PropTypes.number,
  countryDetailsCount: PropTypes.number
};