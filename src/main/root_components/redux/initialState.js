import {statisticsInitialState} from "../../domain_components/statistics/redux";

export const initialState = {
  statistics: statisticsInitialState,
  routing: {}
};