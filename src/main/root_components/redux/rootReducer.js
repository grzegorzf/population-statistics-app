import {combineReducers} from 'redux'
import {routerReducer} from 'react-router-redux';
import {statisticsReducer} from "../../domain_components";

export const rootReducer = combineReducers({
  routing: routerReducer,
  statistics: statisticsReducer
});
